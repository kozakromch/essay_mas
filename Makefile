all: build run

build:
	latexmk -xelatex \
	-synctex=1 main.tex
	
run:
	xreader main.pdf &
	
clean:
	rm *.aux \
	*.fdb_latexmk \
	*.fls \
	*.log \
	*.out \
	*.synctex.gz \
	*.toc
